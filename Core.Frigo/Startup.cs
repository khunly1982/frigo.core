using Core.Frigo.Abstractions;
using Core.Frigo.Global.Entities;
using Core.Frigo.Global.Mocks;
using Core.Frigo.Global.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Core.Frigo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Ajouter au ServiceLocator la session
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromSeconds(86400);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            // Ajouter au serviceLocator un service qui permet de r�cup�rer l'HttpContext
            services.AddHttpContextAccessor();

            services.AddControllersWithViews();

            // ajouter au serviceLocator nos services de connection db
            services.AddScoped<IDbConnection>((p) => new SqlConnection(
                Configuration.GetConnectionString("default")
            ));
            services.AddScoped<IProductRepository<Product>, ProductService>();
            services.AddScoped<IContentRepository<Content>, ContentService>();
            services.AddScoped<IUserRepository<User>, UserService>();

            // ajouter au SL le service de connection aux API's
            services.AddScoped<HttpClient>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
