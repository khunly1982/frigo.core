﻿using Core.Frigo.Abstractions;
using Core.Frigo.Global.Entities;
using Core.Frigo.Infrastucture;
using Core.Frigo.Models;
using Core.Frigo.Models.Forms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Frigo.Controllers
{
    [Authorization]
    public class ProductController : Controller
    {
        private IProductRepository<Product> _pRepo;

        // injection de dépendance par constructeur
        public ProductController(IProductRepository<Product> pRepo)
        {
            _pRepo = pRepo;
        }

        
        public ActionResult Index()
        {
            if(TempData["message"] != null)
            {
                ViewBag.Message = TempData["message"];
                ViewBag.Severity = TempData["severity"];
            }

            IEnumerable<Product> products = _pRepo.Get(SessionManager.Get<User>(HttpContext.Session).Id);
            IEnumerable<ProductViewModel> model = products
                .Select(p => p.MapTo<Product,ProductViewModel>());

            return View(model);
        }

        public ActionResult Details(int id)
        {
            Product p = _pRepo.Get(id, SessionManager.Get<User>(HttpContext.Session).Id);
            if (p is null)
                return NotFound();

            //transform le product -> ProductViewModel
            ProductViewModel model = p.MapTo<Product, ProductViewModel>();
            return View(model);
        }

        // /product/delete/42
        public ActionResult Delete(int id)
        {
            if(!_pRepo.Delete(id, SessionManager.Get<User>(HttpContext.Session).Id))
            {
                // message d'erreur
                TempData["message"] = "Suppression impossible";
                TempData["severity"] = "error";
            }
            return RedirectToAction("Index");
        }

        // afficher le formulaire
        public ActionResult Create()
        {
            return View();
        }

        // traiter le formulaire
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductForm model)
        {
            // validation du formulaire
            // si form valide
            if(ModelState.IsValid)
            {
                // alternative a l'attribute Unique ProductAttribute
                //if(!_pRepo.ProductNotExists(
                //    model.Name, 
                //    SessionManager.Get<User>(HttpContext.Session).Id)
                //)
                //{
                //    ModelState.AddModelError("Name", "Ce produit existe déjà");
                //    return View(model);
                //}

                Product p = model.MapTo<ProductForm, Product>();
                //Product p = new Product { Name = model.Name };
                p.UserId = SessionManager.Get<User>(HttpContext.Session).Id;
                // insertion
                if(_pRepo.Insert(p))
                {
                    // message
                    TempData["message"] = "Insertion OK";
                    TempData["severity"] = "success";
                }
                else
                {
                    TempData["message"] = "Une erreur une survenue";
                    TempData["severity"] = "error";
                }

                // redirection
                return RedirectToAction("Index");
            }
            else
            {
                // sinon
                return View(model);
            }
        }
    }
}
