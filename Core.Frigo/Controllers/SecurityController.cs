﻿using Core.Frigo.Abstractions;
using Core.Frigo.Global.Entities;
using Core.Frigo.Infrastucture;
using Core.Frigo.Models.Forms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Core.Frigo.Controllers
{
    public class SecurityController : Controller
    {
        private readonly IUserRepository<User> _uRepo;

        public SecurityController(IUserRepository<User> uRepo)
        {
            _uRepo = uRepo;
        }

        public IActionResult Login()
        {
            // afficher le formulaire de connection
            return View();
        }

        [HttpPost]
        public IActionResult Login(LoginForm model)
        {
            // Validation du formulaire
            if(ModelState.IsValid)
            {
                // checker les infos dans la db
                User u = _uRepo.CheckUser(model.Email, model.Password);
                if(u != null)
                {
                    //ok
                    // enregistrer en session l'utilisateur
                    SessionManager.Set(HttpContext.Session, u);
                    // rediriger
                    return RedirectToAction("Index", "Product");
                }
                else
                {
                    // afficher un message d'erreur
                    ViewBag.Message = "La combinaison email / password n'est pas correcte";
                    ViewBag.Severity = "error";
                }
            }
            return View(model);
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }
    }
}
