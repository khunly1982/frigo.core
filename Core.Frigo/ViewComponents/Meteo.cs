﻿using Core.Frigo.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Core.Frigo.ViewComponents
{
    public class Meteo : ViewComponent
    {
        private string apiUrl;

        private string apiKey;

        private string city;

        private HttpClient httpClient;

        public Meteo(HttpClient httpClient)
        {
            this.httpClient = httpClient;
            apiUrl = "https://api.openweathermap.org/data/2.5/weather";
            apiKey = "d52e50e34214ff0b92247f788638eeb9";
            city = "Charleroi";
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            HttpResponseMessage message = await httpClient
                .GetAsync($"{apiUrl}?q={city}&APPID={apiKey}&units=metric");
            if(message.IsSuccessStatusCode)
            {
                string json = await message.Content.ReadAsStringAsync();
                MeteoViewModel model = JsonConvert.DeserializeObject<MeteoViewModel>(json);
                return View(model);
            }
            return View(null);
        }
    }
}
