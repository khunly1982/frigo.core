﻿using Core.Frigo.Infrastucture;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Frigo.Models.Forms
{
    public class ProductForm
    {
        [Required(ErrorMessage = "Le champs est requis")]
        [MaxLength(150)]
        [MinLength(2)]
        [Display(Name = "Nom : ")]
        [UniqueProduct]
        public string Name { get; set; }
    }
}
