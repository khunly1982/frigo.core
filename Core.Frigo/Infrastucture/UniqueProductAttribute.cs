﻿using Core.Frigo.Abstractions;
using Core.Frigo.Global.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Frigo.Infrastucture
{
    public class UniqueProductAttribute : ValidationAttribute
    {
        private IProductRepository<Product> _repo;
        private ISession _session;

        public UniqueProductAttribute()
            : base("Ce produit existe déjà")
        {

        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // à la connaissance de Khun, impossible de répérer une instance d'un service autrement
            _repo = (IProductRepository<Product>)
                validationContext.GetService(typeof(IProductRepository<Product>));
            IHttpContextAccessor ca =
                (IHttpContextAccessor)validationContext.GetService(typeof(IHttpContextAccessor));
            _session = ca.HttpContext.Session;
            
            return base.IsValid(value, validationContext);
        }

        public override bool IsValid(object value)
        {
            return _repo.ProductNotExists((string)value, SessionManager.Get<User>(_session).Id);
        }
    }
}
