﻿using Core.Frigo.Global.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Frigo.Infrastucture
{
    public class AuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            // Vérifier dans la session l'identité de l'utilisateur
            User u = SessionManager.Get<User>(context.HttpContext.Session);
            //User u = context.HttpContext.Session.Get<User>();
            // si pas d'utilisateur 
            // rediger vers la page de login ou erreur 401 si pas d'utilisateur
            if(u is null)
            {
                // pour 401
                //context.Result = new UnauthorizedResult();
                //pour la redirection
                context.Result = new RedirectToActionResult("Login", "Security", null);
            }
        }
    }
}
