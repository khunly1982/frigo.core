﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Core.Frigo.Infrastucture
{
    public static class MapperExtensions
    {
        public static TResult MapTo<TFrom, TResult>(this TFrom from)
            where TResult : new()
        {
            // création d'une instance vide de l'objet final
            TResult result = new TResult();

            // récuperation de toutes les propriété de mon type final
            PropertyInfo[] properties = typeof(TResult).GetProperties();

            foreach (PropertyInfo resultProp in properties)
            {
                // récuration de la propriété qui possède le même nom dans le type de départ
                PropertyInfo fromProp = typeof(TFrom).GetProperty(resultProp.Name);
                if (fromProp != null)
                {
                    // répération de la valeur de la propriété de l'objet de départ 
                    object value = fromProp.GetValue(from);
                    if(resultProp.PropertyType.IsAssignableFrom(fromProp.PropertyType))
                    {
                        // assignation de la valeur récupérée vers l'objet final
                        resultProp.SetValue(result, value);
                    }
                }
            }

            return result;
        }
    }
}
