﻿using Core.Frigo.Global.Entities;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Frigo.Infrastucture
{
    public static class SessionManager
    {
        /// <summary>
        /// Récupérer le user en session
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        public static T Get<T>(ISession session)
        {
            // déserialiser string -> User
            string chaine = session.GetString(typeof(T).Name);
            // return User
            if (chaine is null) return default;
            return JsonConvert.DeserializeObject<T>(chaine);
        }

        /// <summary>
        /// Enregistrer l'utilisateur en session
        /// </summary>
        /// <param name="session"></param>
        /// <param name="user"></param>
        public static void Set<T>(ISession session, T user)
        {
            // sérialiser le user -> string
            string serialized = JsonConvert.SerializeObject(user);
            session.SetString(typeof(T).Name, serialized);
        }
    }
}
