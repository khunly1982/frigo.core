﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Frigo.Abstractions
{
    public interface IContentRepository<TEntity>
    {
        IEnumerable<TEntity> Get(int userId);
        bool Insert(TEntity entity);
        bool Delete(long id, int userId);
    }
}
