﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Frigo.Abstractions
{
    public interface IProductRepository<TEntity>
    {
        IEnumerable<TEntity> Get(int userId);
        TEntity Get(int id, int userId);
        bool Insert(TEntity entity);
        bool ProductInUse(int id, int userId);
        bool ProductNotExists(string name, int userId);
        bool Update(int id, TEntity entity);
        bool Delete(int id, int userId);
    }
}
