﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Frigo.Abstractions
{
    public interface IUserRepository<TEntity>
    {
        TEntity CheckUser(string email, string passwd);
        bool EmailNotExists(string email);
        bool Register(TEntity entity);
    }
}
