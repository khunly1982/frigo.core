﻿using Core.Frigo.Global.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Core.Frigo.Global.Mappers
{
    static class ReaderExtensions
    {
        public static Product ToProduct(this IDataReader r)
        {
            return new Product
            {
                Id = (int)r["Id"],
                Name = (string)r["Name"],
                UserId = (int)r["UserId"]
            };
        }

        public static User ToUser(this IDataReader r)
        {
            return new User
            {
                Id = (int)r["Id"],
                LastName = (string)r["LastName"],
                FirstName = (string)r["FirstName"],
                Email = (string)r["Email"]
            };
        }

        public static Content ToContent(this IDataReader r)
        {
            return null;
        }
    }
}
