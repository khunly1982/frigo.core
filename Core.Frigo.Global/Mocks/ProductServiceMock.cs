﻿using Core.Frigo.Abstractions;
using Core.Frigo.Global.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Frigo.Global.Mocks
{
    public class ProductServiceMock : IProductRepository<Product>
    {
        private static IEnumerable<Product> _fakedb;

        static ProductServiceMock() {
            _fakedb = new List<Product> { 
                new Product { Id = 1, Name = "Coca", UserId = 1 },
                new Product { Id = 2, Name = "Fanta", UserId = 1 },
                new Product { Id = 3, Name = "Dr Pepper", UserId = 2 },
            };
        }

        public bool Delete(int id, int userId)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> Get(int userId)
        {
            return _fakedb.Where(p => p.UserId == userId);
        }

        public Product Get(int id, int userId)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Product entity)
        {
            throw new NotImplementedException();
        }

        public bool ProductInUse(int id, int userId)
        {
            throw new NotImplementedException();
        }

        public bool ProductNotExists(string name, int userId)
        {
            throw new NotImplementedException();
        }

        public bool Update(int id, Product entity)
        {
            throw new NotImplementedException();
        }
    }
}
