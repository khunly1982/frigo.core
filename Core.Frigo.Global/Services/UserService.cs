﻿using Core.Frigo.Abstractions;
using Core.Frigo.Global.Entities;
using Core.Frigo.Global.Mappers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Core.Frigo.Global.Services
{
    public class UserService : BaseService, IUserRepository<User>
    {
        public UserService(IDbConnection connection)
            : base(connection)
        {
            
        }

        public User CheckUser(string email, string passwd)
        {
            _connection.Open();
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = "CSP_CheckUser";
            cmd.CommandType = CommandType.StoredProcedure;
            AddParameter(cmd, "Email", email);
            AddParameter(cmd, "Passwd", passwd);

            IDataReader reader = cmd.ExecuteReader();
            User u = null;
            if(reader.Read())
            {
                u = reader.ToUser();
            } 
            _connection.Close();
            return u;
        }

        public bool EmailNotExists(string email)
        {
            throw new NotImplementedException();
        }

        public bool Register(User entity)
        {
            throw new NotImplementedException();
        }
    }
}
