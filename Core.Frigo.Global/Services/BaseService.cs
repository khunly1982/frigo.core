﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Core.Frigo.Global.Services
{
    public abstract class BaseService
    {
        protected readonly IDbConnection _connection;

        protected BaseService(IDbConnection connection)
        {
            _connection = connection;
        }

        protected void AddParameter(IDbCommand cmd, string parameterName, object value)
        {
            IDataParameter p = cmd.CreateParameter();
            p.ParameterName = parameterName;
            p.Value = value ?? DBNull.Value;
            cmd.Parameters.Add(p);
        }
    }
}
