﻿using Core.Frigo.Abstractions;
using Core.Frigo.Global.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using Core.Frigo.Global.Mappers;

namespace Core.Frigo.Global.Services
{
    public class ProductService : BaseService, IProductRepository<Product>
    {

        public ProductService(IDbConnection connection)
            : base(connection)
        {
            
        }

        public bool Delete(int id, int userId)
        {
            _connection.Open();
            string query = "CSP_DeleteProduct";
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = query;

            IDbDataParameter p1 = cmd.CreateParameter();
            p1.ParameterName = "Id";
            p1.Value = id;
            cmd.Parameters.Add(p1);

            IDbDataParameter p2 = cmd.CreateParameter();
            p2.ParameterName = "UserId";
            p2.Value = userId;
            cmd.Parameters.Add(p2);

            cmd.CommandType = CommandType.StoredProcedure;

            int nbLignes = cmd.ExecuteNonQuery();
            _connection.Close();
            return nbLignes != 0;
        }

        public IEnumerable<Product> Get(int userId)
        {
            //ouvrir la conn
            _connection.Open();

            // créer la commande + parametres + query + strore proc ?
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM Product WHERE userId = @p1";

            IDataParameter p1 = cmd.CreateParameter();
            p1.ParameterName = "p1";
            p1.Value = userId;
            cmd.Parameters.Add(p1);
            //List<Product> l = new List<Product>();
            // éxécuter la commande
            IDataReader r = cmd.ExecuteReader();
            while(r.Read())
            {
                yield return r.ToProduct();
            }

            //fermer la conn
            _connection.Close();
            //return l;
        }

        public Product Get(int id, int userId)
        {
            // ouvrir la connection
            _connection.Open();
            // definir et créer la commande
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = 
            @"SELECT * FROM [Product] 
            WHERE Id = @p1 AND UserId = @p2";

            //IDataParameter p1 = cmd.CreateParameter();
            //p1.ParameterName = "p1";
            //p1.Value = id;
            //cmd.Parameters.Add(p1);

            //IDataParameter p2 = cmd.CreateParameter();
            //p2.ParameterName = "p2";
            //p2.Value = userId;
            //cmd.Parameters.Add(p2);

            AddParameter(cmd, "p1", id);
            AddParameter(cmd, "p2", userId);

            // exécuter la commande
            IDataReader r = cmd.ExecuteReader();
            Product result = null;
            if(r.Read())
            {
                result = r.ToProduct();
            }

            // fermer la conn
            _connection.Close();
            return result;
        }

        public bool Insert(Product entity)
        {
            _connection.Open();
            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = "CSP_AddProduct";
            cmd.CommandType = CommandType.StoredProcedure;
            AddParameter(cmd, "Name", entity.Name);
            AddParameter(cmd, "UserId", entity.UserId);
            int nbLines = cmd.ExecuteNonQuery();
            _connection.Close();
            return nbLines != 0;
        }

        public bool ProductInUse(int id, int userId)
        {
            throw new NotImplementedException();
        }

        public bool ProductNotExists(string name, int userId)
        {
            _connection.Open();

            IDbCommand cmd = _connection.CreateCommand();
            cmd.CommandText = @"
                SELECT [Name] FROM Product 
                WHERE [Name] = @name AND UserId = @userId
            ";
            AddParameter(cmd, "name", name);
            AddParameter(cmd, "userId", userId);
            object result = cmd.ExecuteScalar();
            _connection.Close();

            return result == null;
        }

        public bool Update(int id, Product entity)
        {
            throw new NotImplementedException();
        }


        
    }
}
